export default [{
  path: '/parameters/',
  component: require('./components/pages/Parameters.vue'),
  name: 'about'
},
{
  path: '/login/',
  component: require('./components/pages/login.vue'),
  name: 'login'
},
{
  path: '/activities/',
  component: require('./components/pages/activities.vue'),
  name: 'activities'
},
{
  path: '/instructions/',
  component: require('./components/pages/instructions.vue'),
  name: 'instructions'
},
{
  path: '/punchclock/',
  component: require('./components/pages/punchclock.vue'),
  name: 'punchclock'
},
{
  path: '/incident/',
  component: require('./components/pages/incident.vue'),
  name: 'incident'
},
{
  path: '/daybook/',
  component: require('./components/pages/daybook.vue'),
  name: 'daybook'
},
{
  path: '/mydocuments/',
  component: require('./components/pages/mydocuments.vue'),
  name: 'mydocuments'
},
{
  path: '/operations/',
  component: require('./components/pages/operations.vue'),
  name: 'operations'
}
]

